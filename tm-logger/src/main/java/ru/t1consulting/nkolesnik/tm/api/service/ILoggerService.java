package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ILoggerService {

    void writeLog(@NotNull String message);
}
