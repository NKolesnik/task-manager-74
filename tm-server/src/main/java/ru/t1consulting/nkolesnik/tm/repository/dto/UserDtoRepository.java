package ru.t1consulting.nkolesnik.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDto;

@Repository
public interface UserDtoRepository extends AbstractDtoRepository<UserDto> {

    @Nullable
    @Query("SELECT u FROM UserDto u WHERE u.login=:login")
    UserDto findByLogin(
            @Nullable @Param("login") final String login
    );

    @Nullable
    @Query("SELECT u FROM UserDto u WHERE u.email=:email")
    UserDto findByEmail(
            @Nullable @Param("email") final String email
    );

    @NotNull
    @Query("SELECT COUNT (1) = 1 FROM UserDto u WHERE u.login=:login")
    Boolean isLoginExist(
            @Nullable @Param("login") final String login
    );

    @NotNull
    @Query("SELECT COUNT (1) = 1 FROM UserDto u WHERE u.email=:email")
    Boolean isEmailExist(
            @Nullable @Param("email") final String email
    );

    @Modifying
    @Query("UPDATE UserDto u SET u.locked=true where u.login=:login")
    void lockUserByLogin(
            @Nullable @Param("login") final String login
    );

    @Modifying
    @Query("UPDATE UserDto u SET u.locked=false where u.login=:login")
    void unlockUserByLogin(
            @Nullable @Param("login") final String login
    );

    @Modifying
    @Query("UPDATE UserDto u " +
            "SET u.firstName=:firstName, u.middleName=:middleName, u.lastName=:lastName " +
            "WHERE u.id=:id")
    void updateUser(
            @Nullable @Param("id") final String id,
            @Nullable @Param("firstName") final String firstName,
            @Nullable @Param("middleName") final String middleName,
            @Nullable @Param("lastName") final String lastName
    );

    @Modifying
    @Query("UPDATE UserDto u SET u.passwordHash=:passwordHash WHERE u.id=:id")
    void setPassword(
            @Nullable @Param("id") final String id,
            @Nullable @Param("passwordHash") final String passwordHash
    );

    @Modifying
    @Query("DELETE FROM UserDto u WHERE u.login=:login")
    void removeByLogin(
            @Nullable @Param("login") final String login
    );

}
