package ru.t1consulting.nkolesnik.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.model.Task;

import java.util.List;

@Repository
public interface TaskRepository extends AbstractUserOwnedRepository<Task> {
    @Nullable
    @Query("SELECT t FROM Task t WHERE t.id =:id AND t.user.id =:userId")
    Task findById(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("id") final String id
    );

    @NotNull
    @Query("SELECT t FROM Task t WHERE t.user.id =:userId")
    List<Task> findAll(
            @Nullable @Param("userId") final String userId
    );

    @NotNull
    @Query("SELECT t FROM Task t WHERE t.user.id=:userId ORDER BY :sortOrder")
    List<Task> findAll(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("sortOrder") final String sortOrder
    );

    @NotNull
    @Query("SELECT t FROM Task t WHERE t.user.id =:userId AND t.project.id =:projectId")
    List<Task> findAllByProjectId(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("projectId") final String projectId
    );

    @Query("SELECT COUNT(1) = 1 FROM Task t WHERE t.id =:id AND t.user.id =:userId")
    boolean existsById(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("id") final String id
    );

    @Query("SELECT COUNT(t) FROM Task t WHERE t.user.id =:userId")
    long getSize(
            @Nullable @Param("userId") final String userId
    );

    @Modifying
    @Query("DELETE FROM Task t WHERE t.user.id =:userId AND t.id =:id")
    void removeById(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("id") final String id
    );

    @Modifying
    @Query("DELETE FROM Task t WHERE t.project.id =:projectId")
    void removeByProjectId(
            @Nullable @Param("projectId") final String projectId
    );

    @Modifying
    @Query("DELETE FROM Task t WHERE t.project.id =:projectId AND t.user.id =:userId")
    void removeByProjectId(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("projectId") final String projectId
    );

    @Modifying
    @Query("DELETE FROM Task t WHERE t.user.id =:userId")
    void clear(
            @Nullable @Param("userId") final String userId
    );

}
