package ru.t1consulting.nkolesnik.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IProjectDtoService;
import ru.t1consulting.nkolesnik.tm.exception.project.ProjectEmptyIdException;
import ru.t1consulting.nkolesnik.tm.exception.project.ProjectNullException;
import ru.t1consulting.nkolesnik.tm.exception.user.UserEmptyIdException;
import ru.t1consulting.nkolesnik.tm.model.dto.ProjectDto;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectDtoService implements IProjectDtoService {

    @NotNull
    @Autowired
    private IProjectDtoRepository projectRepository;

    @Modifying
    @Transactional
    public void add(@Nullable final String userId, @Nullable final ProjectDto project) {
        Optional.ofNullable(userId).orElseThrow(UserEmptyIdException::new);
        Optional.ofNullable(project).orElseThrow(ProjectNullException::new);
        project.setUserId(userId);
        projectRepository.saveAndFlush(project);
    }

    @Override
    @Modifying
    @Transactional
    public ProjectDto create(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserEmptyIdException::new);
        @NotNull final ProjectDto project = new ProjectDto();
        project.setUserId(userId);
        project.setName("New Project " + System.currentTimeMillis());
        projectRepository.saveAndFlush(project);
        return project;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserEmptyIdException::new);
        Optional.ofNullable(id).orElseThrow(ProjectEmptyIdException::new);
        return projectRepository.existsByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    public ProjectDto findById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserEmptyIdException::new);
        Optional.ofNullable(id).orElseThrow(ProjectEmptyIdException::new);
        return projectRepository.findByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    public List<ProjectDto> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserEmptyIdException::new);
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public long count(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserEmptyIdException::new);
        return projectRepository.countByUserId(userId);
    }

    @Override
    @Modifying
    @Transactional
    public void save(@Nullable final String userId, @Nullable final ProjectDto project) {
        Optional.ofNullable(userId).orElseThrow(UserEmptyIdException::new);
        Optional.ofNullable(project).orElseThrow(ProjectNullException::new);
        project.setUserId(userId);
        projectRepository.saveAndFlush(project);
    }

    @Override
    @Modifying
    @Transactional
    public void deleteById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserEmptyIdException::new);
        Optional.ofNullable(id).orElseThrow(ProjectEmptyIdException::new);
        projectRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Modifying
    @Transactional
    public void delete(@Nullable final String userId, @Nullable final ProjectDto project) {
        Optional.ofNullable(userId).orElseThrow(UserEmptyIdException::new);
        Optional.ofNullable(project).orElseThrow(ProjectNullException::new);
        deleteById(userId, project.getId());
    }

    @Override
    @Modifying
    @Transactional
    public void deleteAll(@Nullable final String userId, @Nullable final List<ProjectDto> projectList) {
        Optional.ofNullable(userId).orElseThrow(UserEmptyIdException::new);
        Optional.ofNullable(projectList).orElseThrow(ProjectNullException::new);
        projectList.forEach(project -> deleteById(userId, project.getId()));
    }

    @Override
    @Modifying
    @Transactional
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserEmptyIdException::new);
        projectRepository.deleteByUserId(userId);
    }

}
