package ru.t1consulting.nkolesnik.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1consulting.nkolesnik.tm.model.dto.ProjectDto;

public interface ProjectDtoRepository extends JpaRepository<ProjectDto, String> {

}
