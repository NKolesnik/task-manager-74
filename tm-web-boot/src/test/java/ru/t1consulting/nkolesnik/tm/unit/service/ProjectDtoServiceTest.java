package ru.t1consulting.nkolesnik.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import ru.t1consulting.nkolesnik.tm.api.service.IProjectDtoService;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.project.ProjectEmptyIdException;
import ru.t1consulting.nkolesnik.tm.exception.project.ProjectNullException;
import ru.t1consulting.nkolesnik.tm.exception.user.UserEmptyIdException;
import ru.t1consulting.nkolesnik.tm.marker.UnitCategory;
import ru.t1consulting.nkolesnik.tm.model.dto.ProjectDto;
import ru.t1consulting.nkolesnik.tm.util.UserUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class ProjectDtoServiceTest {

    @NotNull
    private static String userId;

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Nullable
    private ProjectDto project;

    @Before
    public void setUp() throws Exception {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        projectService.clear(userId);
        project = projectService.create(userId);
    }

    @After
    public void tearDown() throws Exception {
        projectService.clear(userId);
    }

    @Test
    public void create() {
        projectService.clear(userId);
        Assert.assertThrows(UserEmptyIdException.class, () -> projectService.create(null));
        @NotNull final ProjectDto projectDto = projectService.create(userId);
        Assert.assertNotNull(projectDto);
    }

    @Test
    public void existsById() {
        Assert.assertThrows(UserEmptyIdException.class, () -> projectService.existsById(null, project.getId()));
        Assert.assertThrows(ProjectEmptyIdException.class, () -> projectService.existsById(userId, null));
        boolean exists = projectService.existsById(userId, project.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void findById() {
        Assert.assertThrows(UserEmptyIdException.class, () -> projectService.findById(null, project.getId()));
        Assert.assertThrows(ProjectEmptyIdException.class, () -> projectService.findById(userId, null));
        @Nullable final ProjectDto projectDto = projectService.findById(userId, project.getId());
        Assert.assertNotNull(projectDto);
    }

    @Test
    public void findAll() {
        Assert.assertThrows(UserEmptyIdException.class, () -> projectService.findAll(null));
        @Nullable final List<ProjectDto> projects = projectService.findAll(userId).stream().collect(Collectors.toList());
        Assert.assertNotNull(projects);
    }

    @Test
    public void count() {
        Assert.assertThrows(UserEmptyIdException.class, () -> projectService.count(null));
        long count = projectService.count(userId);
        Assert.assertEquals(1L, count);
    }

    @Test
    public void save() {
        Assert.assertThrows(UserEmptyIdException.class, () -> projectService.save(null, project));
        Assert.assertThrows(ProjectNullException.class, () -> projectService.save(userId, null));
        @Nullable ProjectDto projectDto = projectService.findById(userId, project.getId());
        Assert.assertNotNull(projectDto);
        @NotNull final Status newStatus = Status.COMPLETED;
        Assert.assertNotEquals(newStatus, projectDto.getStatus());
        projectDto.setStatus(newStatus);
        projectService.save(userId, projectDto);
        projectDto = projectService.findById(userId, project.getId());
        Assert.assertNotNull(projectDto);
        Assert.assertEquals(projectDto.getStatus(), newStatus);
    }

    @Test
    public void deleteById() {
        Assert.assertThrows(UserEmptyIdException.class, () -> projectService.deleteById(null, project.getId()));
        Assert.assertThrows(ProjectEmptyIdException.class, () -> projectService.deleteById(userId, null));
        @Nullable ProjectDto projectDto = projectService.findById(userId, project.getId());
        Assert.assertNotNull(projectDto);
        projectService.deleteById(userId, projectDto.getId());
        projectDto = projectService.findById(userId, projectDto.getId());
        Assert.assertNull(projectDto);
    }

    @Test
    public void delete() {
        Assert.assertThrows(UserEmptyIdException.class, () -> projectService.delete(null, project));
        Assert.assertThrows(ProjectNullException.class, () -> projectService.delete(userId, null));
        @Nullable ProjectDto projectDto = projectService.findById(userId, project.getId());
        Assert.assertNotNull(projectDto);
        projectService.delete(userId, project);
        projectDto = projectService.findById(userId, projectDto.getId());
        Assert.assertNull(projectDto);
    }

    @Test
    public void deleteAll() {
        List<ProjectDto> projects = new ArrayList<>();
        projects.add(project);
        Assert.assertThrows(UserEmptyIdException.class, () -> projectService.deleteAll(null, projects));
        Assert.assertThrows(ProjectNullException.class, () -> projectService.deleteAll(userId, null));
        @Nullable ProjectDto projectDto = projectService.findById(userId, project.getId());
        Assert.assertNotNull(projectDto);
        projectService.deleteAll(userId, projects);
        projectDto = projectService.findById(userId, projectDto.getId());
        Assert.assertNull(projectDto);
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserEmptyIdException.class, () -> projectService.clear(null));
        long count = projectService.count(userId);
        Assert.assertEquals(1L, count);
        projectService.clear(userId);
        count = projectService.count(userId);
        Assert.assertEquals(0L, count);
    }

}