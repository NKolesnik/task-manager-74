package ru.t1consulting.nkolesnik.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    @NotNull
    public static final String PATTERN = "yyyy-MM-dd";

    @NotNull
    public static SimpleDateFormat FORMATTER = new SimpleDateFormat(PATTERN);

    @Nullable
    public static Date toDate(@NotNull final String value) {
        try {
            return FORMATTER.parse(value);
        } catch (@NotNull final ParseException e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    @NotNull
    public static String toString(@Nullable final Date value) {
        if (value == null) return "";
        return FORMATTER.format(value);
    }

}

